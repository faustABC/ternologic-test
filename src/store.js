import { stateUtils, reducers } from "common"
import { projectsReducer } from "./features/project-list/state/reducers"

/**
 * users - all users of the app. Currently contains only one current user
 * statuses - project statuses
 * projects - all projects
 */
export const rootReducer = stateUtils.combineReducers({
  users: reducers.userReducer,
  statuses: reducers.statusReducer,
  projects: projectsReducer,
})
