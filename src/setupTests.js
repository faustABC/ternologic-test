// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import "@testing-library/jest-dom/extend-expect"
import { initI18n, TokenSerializer } from "utils"
import * as resources from "locale"
import dotenv from "dotenv"
import { ACCESS_TOKEN_NAME } from "configuration"

beforeAll(() => {
  dotenv.config({ path: ".env.development" })
  const tokenSerializer = new TokenSerializer(localStorage, ACCESS_TOKEN_NAME)
  tokenSerializer.serializeToken("")
  initI18n(resources)
})
