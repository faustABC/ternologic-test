/**
 * @class validation errors generalizing root class
 * @extends Error
 */
export class ValidationError extends Error {
  /**
   *
   * @param {string} validatedProperty property that was validated when validation failed
   * @param {string} expectedResult result of validation that was expected
   * @param  {...any} props other props to Error constructor
   */
  constructor(validatedProperty, expectedResult, ...props) {
    super(...props)

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ValidationError)
    }

    this.date = new Date()
    this.validatedProperty = validatedProperty
    this.expectedResult = expectedResult
  }
}
