/**
 * @class General error class
 * @extends Error
 * @classdesc used for generalizing of all Errors as root HTTP Error
 */
export class HTTPError extends Error {
  /**
   * Constructor for HTTPError class
   * @param {string} href url of resource that failed to load
   * @param {number} status status of HTTP request
   * @param  {...any} props props to the Error constructor
   */
  constructor(href, status, ...props) {
    super(...props)

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, HTTPError)
    }

    this.date = new Date()
    this.href = href
    this.status = status
  }
}
