import React from "react"
import { fireEvent, render } from "@testing-library/react"
import AuthSuggestion from "./App"
import { ROUTING } from "configuration"

describe("AuthSuggestion", () => {
  it("All text is localized", () => {
    const { getByText } = render(
      <AuthSuggestion
        signInLink={ROUTING.SIGN_IN}
        signUpLink={ROUTING.SIGN_UP}
      />
    )

    expect(getByText(/sign up/i)).toBeInTheDocument()
    expect(getByText(/sign in/i)).toBeInTheDocument()
  })
  it("Url path is changing after sign up button clicking", () => {
    const { getByText } = render(
      <AuthSuggestion
        signInLink={ROUTING.SIGN_IN}
        signUpLink={ROUTING.SIGN_UP}
      />
    )

    fireEvent.click(getByText(/sign up/i))

    expect(location.pathname).toBe(ROUTING.SIGN_UP)
  })
  it("Url path is changing after sign in button clicking", () => {
    const { getByText } = render(
      <AuthSuggestion
        signInLink={ROUTING.SIGN_IN}
        signUpLink={ROUTING.SIGN_UP}
      />
    )

    fireEvent.click(getByText(/sign in/i))

    expect(location.pathname).toBe(ROUTING.SIGN_IN)
  })
})
