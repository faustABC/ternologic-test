import { Button, Paper, Typography } from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import { Link } from "@reach/router"
import React from "react"
import PropTypes from "prop-types"
import { useTranslation } from "react-i18next"

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(2),
    display: "flex",
    height: "100vh",
  },
  paperRoot: {
    margin: "auto",
    width: "min(500px, 100%)",
    alignContent: "center",
    padding: theme.spacing(2),
    display: "grid",
    gridTemplate: "1fr 1fr / 1fr 1fr 1fr",
    gap: theme.spacing(2),
  },
  fullWidth: {
    gridColumn: "1 / 4",
  },
  spanLeft: {
    gridColumn: "1 / span 2",
  },
}))

/**
 * Page to purpose user to sign up or log in
 * @param {string} params.signUpLink link to sign up page
 * @param {string} params.signInLink link to sign in page
 */
function App({ signUpLink, signInLink }) {
  const classes = useStyles()
  const { t } = useTranslation()

  return (
    <div className={classes.root}>
      <Paper className={classes.paperRoot}>
        <Typography className={classes.fullWidth} variant="button">
          {t("common:title")}
        </Typography>
        <Button
          className={classes.spanLeft}
          variant="contained"
          component={Link}
          to={signUpLink}
          color="primary"
        >
          {t("auth:signUp")}
        </Button>
        <Button to={signInLink} variant="outlined" component={Link}>
          {t("auth:signIn")}
        </Button>
      </Paper>
    </div>
  )
}

App.propTypes = {
  signUpLink: PropTypes.string.isRequired,
  signInLink: PropTypes.string.isRequired,
}

export default App
