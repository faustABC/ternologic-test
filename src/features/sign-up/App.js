import React, { useCallback, useEffect } from "react"
import { Layout, TextInput, PasswordInput, useSelector, Button } from "common"
import { makeStyles, Paper, Typography } from "@material-ui/core"
import { useForm } from "hooks"
import { useNavigate } from "@reach/router"
import { ROUTING } from "configuration"
import { useTranslation } from "react-i18next"
import { useEffects } from "./effects"

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    gap: theme.spacing(2),
  },
  paper: {
    width: "min(500px, 100%)",
    padding: theme.spacing(2),
    height: "fit-content",
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    gap: theme.spacing(2),
  },
  itemFullWidth: {
    gridColumn: "1 / 3",
  },
}))

/**
 * Displays sign up page with user data inputs
 */
function App() {
  const { t } = useTranslation()
  const classes = useStyles()
  const { signUp } = useEffects()

  const validation = useCallback(({ blurred, values, touched, newErrors }) => {
    if (blurred.email && !/.+@.+/.test(values.email)) {
      newErrors.email.push(t("validationRules:email"))
    }
    if (blurred.reenteredEmail && values.email !== values.reenteredEmail) {
      newErrors.reenteredEmail.push(t("validationRules:reenteredEmail"))
    }
    if (touched.password) {
      if (!/[a-z]/.test(values.password)) {
        newErrors.password.push(t("validationRules:passwordLowercase"))
      }
      if (!/[A-Z]/.test(values.password)) {
        newErrors.password.push(t("validationRules:passwordUppercase"))
      }
      if (!/\d/.test(values.password)) {
        newErrors.password.push(t("validationRules:passwordOneNumber"))
      }
      if (!/.{8}/.test(values.password)) {
        newErrors.password.push(t("validationRules:passwordLength"))
      }
      if (!/[*.!@$%^&(){}[\]:;<>,.?~_/+\-=|\\]/.test(values.password)) {
        newErrors.password.push(t("validationRules:passwordSpecialChar"))
      }
    }
    if (
      blurred.reenteredPassword &&
      values.password !== values.reenteredPassword
    ) {
      newErrors.reenteredPassword.push(t("validationRules:reenteredPassword"))
    }
    return newErrors
  }, [])

  const { inputProps, isAllFilled, isNoErrors, data } = useForm(
    {
      firstName: "",
      lastName: "",
      email: "",
      reenteredEmail: "",
      password: "",
      reenteredPassword: "",
    },
    validation
  )

  const onSubmit = (e) => {
    e.preventDefault()
    signUp(data.values)
  }

  const { current, isLoading, error } = useSelector((state) => state.users)
  const navigate = useNavigate()

  useEffect(() => {
    current && navigate(ROUTING.ROOT)
  }, [current])

  return (
    <Layout>
      <form className={classes.root} onSubmit={onSubmit}>
        <Paper variant="outlined" className={classes.paper}>
          <Typography variant="overline" className={classes.itemFullWidth}>
            {t("auth:signUp")}
          </Typography>
          <TextInput
            {...inputProps.firstName}
            autoComplete="given-name"
            label={t("auth:firstName")}
          />
          <TextInput
            {...inputProps.lastName}
            autoComplete="family-name"
            label={t("auth:lastName")}
          />
          <TextInput
            {...inputProps.email}
            autoComplete="email"
            label={t("auth:email")}
            className={classes.itemFullWidth}
          />
          <TextInput
            {...inputProps.reenteredEmail}
            autoComplete="email"
            label={t("auth:reenteredEmail")}
            className={classes.itemFullWidth}
          />
          <PasswordInput
            {...inputProps.password}
            autoComplete="new-password"
            label={t("auth:password")}
            className={classes.itemFullWidth}
            errorsInRow
            persistentRules={[
              "One lowercase character",
              "One uppercase character",
              "One number",
              "8 characters minimum",
              "One special character",
            ]}
          />
          <PasswordInput
            {...inputProps.reenteredPassword}
            autoComplete="off"
            label={t("auth:reenteredPassword")}
            className={classes.itemFullWidth}
          />
          <Button
            type="submit"
            color="primary"
            variant="contained"
            style={{ gridColumnStart: 2 }}
            disabled={!isAllFilled || !isNoErrors || isLoading}
            isLoading={isLoading}
          >
            {t("auth:signUp")}
          </Button>
        </Paper>
        {error && (
          <Paper variant="outlined" className={classes.paper}>
            <Typography color="error" variant="caption">
              {error}
            </Typography>
          </Paper>
        )}
      </form>
    </Layout>
  )
}

export default App
