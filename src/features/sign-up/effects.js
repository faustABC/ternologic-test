import { actions, useDispatch } from "common"
import { apiClient, tokenSerializer } from "../../App"

export const useEffects = () => {
  const dispatch = useDispatch()

  const signUp = async ({ firstName, lastName, email, password }) => {
    try {
      dispatch(actions.signUp.START())
      const { user, accessToken } = await apiClient.post("sign-up", {
        body: { firstName, lastName, email, password },
      })
      tokenSerializer.serializeToken(accessToken)
      dispatch(actions.signUp.SUCCESS(user))
    } catch (e) {
      dispatch(actions.signUp.ERROR(e.message))
    }
  }

  return { signUp }
}
