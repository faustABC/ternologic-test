import React from "react"
import { fireEvent, render } from "@testing-library/react"
import { rootReducer } from "../../store"
import { StoreProvider } from "common"
import { Router } from "@reach/router"
import { act } from "react-dom/test-utils"
import { SignUp } from ".."

describe("SignUp", () => {
  it("Email validation", async () => {
    const { getByLabelText, findByText } = render(
      <StoreProvider rootReducer={rootReducer}>
        <Router>
          <SignUp default />
        </Router>
      </StoreProvider>
    )
    await act(async () => {
      const emailInput = getByLabelText("Email")

      fireEvent.input(emailInput, {
        target: { value: "incorrect value" },
      })
      fireEvent.blur(emailInput)
    })
    expect((await findByText("Email must be valid")).className).toMatch(
      "Mui-error"
    )
  })
  it("Reentered email must be the same with email", async () => {
    const { getByLabelText, queryByText } = render(
      <StoreProvider rootReducer={rootReducer}>
        <Router>
          <SignUp default />
        </Router>
      </StoreProvider>
    )
    const emailInput = getByLabelText("Email")
    const reenteredEmailInput = getByLabelText("Reenter email")

    await act(async () => {
      fireEvent.input(emailInput, {
        target: { value: "root@root.com" },
      })
      fireEvent.input(reenteredEmailInput, {
        target: { value: "root@root.com" },
      })
      fireEvent.blur(reenteredEmailInput)
    })
    expect(queryByText("Email must be valid")).toBeNull()
  })
  it("Reentered email must be erroring if it's not the same with email", async () => {
    const { getByLabelText, findByText } = render(
      <StoreProvider rootReducer={rootReducer}>
        <Router>
          <SignUp default />
        </Router>
      </StoreProvider>
    )
    const emailInput = getByLabelText("Email")
    const reenteredEmailInput = getByLabelText("Reenter email")

    await act(async () => {
      fireEvent.input(emailInput, {
        target: { value: "root@root.com" },
      })
      fireEvent.input(reenteredEmailInput, {
        target: { value: "another email" },
      })
      fireEvent.blur(reenteredEmailInput)
    })
    expect((await findByText("Emails must be the same")).className).toMatch(
      "Mui-error"
    )
  })
  it("Password input uppercase validation", async () => {
    const { getByLabelText, findByText } = render(
      <StoreProvider rootReducer={rootReducer}>
        <Router>
          <SignUp default />
        </Router>
      </StoreProvider>
    )
    await act(async () => {
      fireEvent.input(getByLabelText("Password"), {
        target: { value: "incorrect value" },
      })
    })
    expect((await findByText("One uppercase character")).className).toMatch(
      "Mui-error"
    )
  })
  it("Password input lowercase validation", async () => {
    const { getByLabelText, findByText } = render(
      <StoreProvider rootReducer={rootReducer}>
        <Router>
          <SignUp default />
        </Router>
      </StoreProvider>
    )
    await act(async () => {
      fireEvent.input(getByLabelText("Password"), {
        target: { value: "INCORRECT VALUE" },
      })
    })
    expect((await findByText("One lowercase character")).className).toMatch(
      "Mui-error"
    )
  })
  it("Password input one number validation", async () => {
    const { getByLabelText, findByText } = render(
      <StoreProvider rootReducer={rootReducer}>
        <Router>
          <SignUp default />
        </Router>
      </StoreProvider>
    )
    await act(async () => {
      fireEvent.input(getByLabelText("Password"), {
        target: { value: "incorrect value" },
      })
    })
    expect((await findByText("One number")).className).toMatch("Mui-error")
  })
  it("Password input special character validation", async () => {
    const { getByLabelText, findByText } = render(
      <StoreProvider rootReducer={rootReducer}>
        <Router>
          <SignUp default />
        </Router>
      </StoreProvider>
    )
    await act(async () => {
      fireEvent.input(getByLabelText("Password"), {
        target: { value: "incorrect value" },
      })
    })
    expect((await findByText("One special character")).className).toMatch(
      "Mui-error"
    )
  })
  it("Password input length validation", async () => {
    const { getByLabelText, findByText } = render(
      <StoreProvider rootReducer={rootReducer}>
        <Router>
          <SignUp default />
        </Router>
      </StoreProvider>
    )
    await act(async () => {
      fireEvent.input(getByLabelText("Password"), {
        target: { value: "value" },
      })
    })
    expect((await findByText("8 characters minimum")).className).toMatch(
      "Mui-error"
    )
  })
})
