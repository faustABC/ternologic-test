import React, { useCallback, useEffect, useMemo, useState } from "react"
import { useSelector, Layout, useDispatch } from "common"
import TopBar from "./components/TopBar"
import ProjectTypeSelectors from "./components/ProjectTypeSelectors"
import ProjectsTable from "./components/ProjectsTable"
import { Button, makeStyles, Typography } from "@material-ui/core"
import { useProjectsEffects } from "./state/misc"
import { setCurrentStatus } from "./state/actions"
import { AVAILABLE_STATUSES } from "fetch/data"
import ActionForm from "./components/ActionForm"
import { dtoToFormProject } from "./utils"
import { useTranslation } from "react-i18next"

const useStyles = makeStyles((theme) => ({
  tableRoot: {
    padding: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    gap: theme.spacing(2),
  },
  tableCaptionRoot: {
    display: "flex",
    justifyContent: "space-between",
  },
}))

const FORM_USE_CASES = {
  EDITING: "EDITING",
  CREATION: "CREATION",
}

/**
 * Used to display and manipulate with projects
 */
function App() {
  const { t } = useTranslation()
  const projects = useSelector((store) => store.projects)
  const { statuses: progressStatuses } = useSelector((store) => store.statuses)
  const {
    getProjectsAsync,
    toggleFormVisibility,
    addProjectAsync,
    getStatusesAsync,
    editProjectAsync,
    deleteProjectAsync,
    getProjectIdToEditingMapSelector,
  } = useProjectsEffects()
  const dispatch = useDispatch()
  const classes = useStyles()
  const [defaultValues, setDefaultValues] = useState()
  const [formUseCase, setFormUseCase] = useState()
  const [formFields, setFormFields] = useState({})
  const [additionalInputProps, setAdditionalInputProps] = useState({})
  const projectIdToEditingMap = useSelector(getProjectIdToEditingMapSelector)
  const [page, setPage] = useState(0)

  useEffect(
    () =>
      setAdditionalInputProps(
        {
          [FORM_USE_CASES.EDITING]: {
            id: { disabled: true },
          },
        }[formUseCase]
      ),
    [formUseCase]
  )

  useEffect(
    () =>
      setFormFields(
        {
          [FORM_USE_CASES.CREATION]: {
            title: t("projects:createTitle"),
            submit: t("projects:createSubmit"),
          },
          [FORM_USE_CASES.EDITING]: {
            title: t("projects:editTitle"),
            submit: t("projects:editSubmit"),
          },
        }[formUseCase] || {}
      ),
    [formUseCase, t]
  )

  useEffect(() => {
    getStatusesAsync()
  }, [])

  useEffect(() => {
    getProjectsAsync()
  }, [projects.currentStatus])

  const onStatusFilterChange = useCallback(
    (status) => {
      setPage(0)
      dispatch(setCurrentStatus(status))
    },
    [dispatch]
  )

  const statuses = useMemo(
    () => [
      { id: AVAILABLE_STATUSES.ALL, text: t("projects:statuses.all") },
      { id: AVAILABLE_STATUSES.MY, text: t("projects:statuses.my") },
      {
        id: AVAILABLE_STATUSES.ACQUIRED,
        text: t("projects:statuses.acquired"),
      },
      { id: AVAILABLE_STATUSES.DRAFTED, text: t("projects:statuses.drafted") },
    ],
    [t]
  )

  const onCreateProject = () => {
    setFormUseCase(FORM_USE_CASES.CREATION)
    toggleFormVisibility()
    setDefaultValues()
  }

  const onEditProject = (id) => {
    const editedProject = projects.all.projects.find(
      (project) => project.id === id
    )
    const formattedProjectForForm = dtoToFormProject(editedProject)
    setFormUseCase(FORM_USE_CASES.EDITING)
    setDefaultValues(formattedProjectForForm)
    toggleFormVisibility()
  }

  const onSubmit =
    formUseCase === FORM_USE_CASES.CREATION ? addProjectAsync : editProjectAsync

  const tableItems = useMemo(
    () =>
      projects.all.projects.map((project) => ({
        ...project,
        isEditable: projectIdToEditingMap[project.id],
      })),
    [projects.all.projects, projectIdToEditingMap]
  )

  return (
    <>
      <TopBar onChange={getProjectsAsync} />
      <Layout margin="normal" maxWidth={undefined}>
        <>
          <ProjectTypeSelectors
            statuses={statuses}
            onChange={onStatusFilterChange}
            currentStatus={projects.currentStatus}
          />
          <div className={classes.tableRoot}>
            <div className={classes.tableCaptionRoot}>
              <Typography variant="button">
                {t("projects:tableCaption")}
              </Typography>
              <Button
                color="primary"
                variant="contained"
                onClick={onCreateProject}
              >
                {t("projects:createTitle")}
              </Button>
            </div>
            <ProjectsTable
              items={tableItems}
              isLoading={projects.all.isLoading}
              onEdit={onEditProject}
              onDelete={deleteProjectAsync}
              page={page}
              setPage={setPage}
            />
          </div>
        </>
      </Layout>
      {projects.isFormVisible && (
        <ActionForm
          isLoading={projects.all.isLoading}
          isVisible={projects.isFormVisible}
          submit={onSubmit}
          handleClose={toggleFormVisibility}
          progressStatuses={progressStatuses}
          defaultValues={defaultValues}
          formFields={formFields}
          additionalInputProps={additionalInputProps}
        />
      )}
    </>
  )
}

export default App
