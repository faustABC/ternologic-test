import { AppBar, fade, InputBase, makeStyles, Toolbar } from "@material-ui/core"
import React, { useEffect, useState } from "react"
import SearchIcon from "@material-ui/icons/Search"
import { FILTER_DEBOUNCE } from "configuration"
import PropTypes from "prop-types"
import { useTranslation } from "react-i18next"
import { LogOutButton } from "common"
import { useDebouncedValue } from "hooks"

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  logOut: {
    marginLeft: "auto",
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}))

/**
 * Top bar of the component
 * @param {Function} params.onChange fires when debounced value of search input is changing
 */
const TopBar = ({ onChange }) => {
  const { t } = useTranslation()
  const classes = useStyles()
  const [filter, setFilter] = useState("")
  const [debouncedValue] = useDebouncedValue(filter, FILTER_DEBOUNCE)
  const onFilterChange = (e) => setFilter(e.target.value)

  useEffect(() => {
    onChange(debouncedValue)
  }, [debouncedValue])

  return (
    <AppBar position="static">
      <Toolbar className={classes.root}>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            value={filter}
            onChange={onFilterChange}
            placeholder={t("common:search")}
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            inputProps={{ "aria-label": "search" }}
          />
        </div>
        <LogOutButton
          iconButtonProps={{ color: "inherit", className: classes.logOut }}
        />
      </Toolbar>
    </AppBar>
  )
}

TopBar.propTypes = {
  onChange: PropTypes.func.isRequired,
}

export default TopBar
