import React from "react"
import PropTypes from "prop-types"
import {
  makeStyles,
  TableCell,
  TableHead as MaterialTableHead,
  TableRow,
  TableSortLabel,
} from "@material-ui/core"

const useStyles = makeStyles({
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
})

/**
 * Used in order to provide table sorting
 * @param {'asc'|'desc'} params.order available order options; asc - ascending, desc - descending
 * @param {string} params.orderBy prop to order by
 * @param {Function} params.onRequestSort fires when user changes sorting
 * @param {object[]} params.headCells cells of table head
 */
function TableHead({ order, orderBy, onRequestSort, headCells }) {
  const classes = useStyles()
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }

  return (
    <MaterialTableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </MaterialTableHead>
  )
}

TableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  headCells: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ).isRequired,
}

export default TableHead
