import React, { useCallback } from "react"
import { TextInput, Button } from "common"
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  InputAdornment,
  makeStyles,
  Paper,
  Typography,
  useMediaQuery,
  useTheme,
} from "@material-ui/core"
import { useForm } from "hooks"
import PropTypes from "prop-types"
import { formatDateToInput, formProjectToDto } from "../utils"
import StatusSelect from "common/components/micro/StatusSelect"
import Status from "common/components/micro/Status"
import CloseIcon from "@material-ui/icons/Close"
import { useTranslation } from "react-i18next"

const useStyles = makeStyles((theme) => ({
  paper: {
    height: "fit-content",
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    gap: theme.spacing(2),
    gridAutoRows: "min-content",
  },
  itemFullWidth: {
    gridColumn: "1 / 3",
  },
  button: {
    margin: theme.spacing(2),
  },
  title: {
    "& > *": {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
  },
}))

/**
 * ActionForm is used for projects editing and creation
 * @param {object} [params.defaultValues={}] values in form which will be treated as default
 * @param {string} [params.progressStatuses=[]] all available project statuses
 * @param {object} [params.additionalInputProps={}] props to TextInput
 * @param {Function} params.submit function executed when form is submitted
 * @param {boolean} params.isLoading indicated whenever form is loading
 * @param {string} [params.networkError] error from network request
 * @param {Function} params.handleClose fires on close
 * @param {object} params.formFields fields map
 */
function ActionForm({
  defaultValues = {},
  progressStatuses = [],
  additionalInputProps = {},
  submit,
  isLoading,
  networkError,
  handleClose,
  formFields,
}) {
  const { t } = useTranslation()
  const classes = useStyles()
  const theme = useTheme()
  const fullScreen = useMediaQuery(theme.breakpoints.down("xs"))

  const validation = useCallback(({ blurred, values, newErrors }) => {
    if (blurred.id && !/^\w{16}$/.test(values.id)) {
      newErrors.id.push(t("validationRules:vIdLength"))
    }
    if (
      blurred.complicity &&
      (!Number.isInteger(+values.complicity) ||
        +values.complicity < 0 ||
        +values.complicity > 100)
    ) {
      newErrors.complicity.push(t("validationRules:complicity"))
    }
    if (
      blurred.price &&
      (!Number.isInteger(+values.price) || +values.price < 0)
    ) {
      newErrors.price.push(t("validationRules:price"))
    }
    return newErrors
  }, [])

  const { inputProps, isAllFilled, isNoErrors, data } = useForm(
    {
      id: defaultValues.id || "",
      name: defaultValues.name || "",
      status: defaultValues.status || "",
      price: defaultValues.price || "",
      provider: defaultValues.provider || "",
      complicity: defaultValues.complicity || "",
      startDate: defaultValues.startDate || formatDateToInput(new Date()),
      deadLine: defaultValues.deadLine || formatDateToInput(new Date()),
      offers: defaultValues.offers || "",
    },
    validation
  )

  const onSubmit = async (e) => {
    e.preventDefault()
    const parsedValues = formProjectToDto(data.values)
    await submit(parsedValues)
    handleClose()
  }

  const mobileClassNames = fullScreen ? classes.itemFullWidth : undefined
  const isSubmitDisabled = !isAllFilled || !isNoErrors || isLoading

  const form = (
    <form onSubmit={onSubmit}>
      <Dialog
        open={true}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        fullScreen={fullScreen}
      >
        <DialogTitle id="alert-dialog-title" className={classes.title}>
          <Typography variant="overline" className={classes.itemFullWidth}>
            {formFields.title}
          </Typography>
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={handleClose}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent className={classes.paper}>
          <TextInput
            {...inputProps.name}
            {...(additionalInputProps.name || {})}
            autoComplete="no"
            label={t("projects:projectName")}
            className={classes.itemFullWidth}
          />
          <TextInput
            {...inputProps.id}
            {...(additionalInputProps.id || {})}
            label={t("projects:virtualId")}
            className={classes.itemFullWidth}
          />
          <StatusSelect
            {...inputProps.status}
            {...(additionalInputProps.status || {})}
            value={inputProps.status.value}
            items={progressStatuses}
            label={t("projects:projectStatus")}
            className={mobileClassNames}
          />

          <TextInput
            {...inputProps.price}
            {...(additionalInputProps.price || {})}
            label={t("projects:price")}
            inputProps={{
              type: "number",
              min: 0,
            }}
            className={mobileClassNames}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">$</InputAdornment>
              ),
              endAdornment: <InputAdornment position="end">k</InputAdornment>,
            }}
          />
          <TextInput
            {...inputProps.provider}
            {...(additionalInputProps.provider || {})}
            label={t("projects:provider")}
            className={mobileClassNames}
          />
          <TextInput
            {...inputProps.complicity}
            {...(additionalInputProps.complicity || {})}
            label={t("projects:complicity")}
            autoComplete="no"
            className={mobileClassNames}
            inputProps={{
              type: "number",
              min: 0,
              max: 100,
            }}
            InputProps={{
              endAdornment: <InputAdornment position="end">%</InputAdornment>,
            }}
          />
          <TextInput
            {...inputProps.startDate}
            {...(additionalInputProps.startDate || {})}
            inputProps={{ min: formatDateToInput(new Date()) }}
            type="date"
            label={t("projects:startDate")}
            className={mobileClassNames}
          />
          <TextInput
            {...inputProps.deadLine}
            {...(additionalInputProps.deadLine || {})}
            inputProps={{ min: formatDateToInput(new Date()) }}
            type="date"
            label={t("projects:deadline")}
            className={mobileClassNames}
          />
          <TextInput
            {...inputProps.offers}
            {...(additionalInputProps.offers || {})}
            label={t("projects:offers")}
            className={classes.itemFullWidth}
            inputProps={{
              type: "number",
              min: 0,
              max: 100,
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={onSubmit}
            className={classes.button}
            type="submit"
            color="primary"
            variant="contained"
            style={{ gridColumnStart: 2 }}
            disabled={isSubmitDisabled}
            isLoading={isLoading}
          >
            {formFields.submit}
          </Button>
        </DialogActions>

        {networkError && (
          <Paper variant="outlined" className={classes.paper}>
            <Typography color="error" variant="caption">
              {networkError}
            </Typography>
          </Paper>
        )}
      </Dialog>
    </form>
  )

  return form
}

ActionForm.propTypes = {
  submit: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  networkError: PropTypes.string,
  defaultValues: PropTypes.object,
  progressStatuses: PropTypes.arrayOf(PropTypes.exact(Status.propTypes)),
  formFields: PropTypes.exact({
    title: PropTypes.string,
    submit: PropTypes.string,
  }),
  additionalInputProps: PropTypes.object,
}

export default ActionForm
