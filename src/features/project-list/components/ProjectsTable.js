import {
  IconButton,
  LinearProgress,
  Menu,
  MenuItem,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
} from "@material-ui/core"
import React, { useCallback, useMemo, useRef, useState } from "react"
import PropTypes from "prop-types"
import { formatDateFactory } from "utils"
import Status from "../../../common/components/micro/Status"
import {
  getComparator,
  percentageFormatter,
  priceFormatter,
  stableSort,
} from "../utils"
import MoreIcon from "@material-ui/icons/MoreVert"
import TableHead from "./TableHead"
import { useTranslation } from "react-i18next"

/**
 * @typedef {object} Project
 * @property {string} id identifier
 * @property {string} name project name
 * @property {Status} status project status
 * @property {number|null} resources project resources
 * @property {number} price price
 * @property {string|null} provider identifier
 * @property {string} complicity identifier
 * @property {Date} startDate identifier
 * @property {Date} deadLine identifier
 * @property {number} offers identifier
 * @property {number} owner owner id
 * @property {boolean} isDraft is project saved
 * @property {number} initialOwner id of owner that created project
 * @property {boolean} isEditable is project can be edited
 */

/**
 * ProjectsTable - table for project displaying.
 * Supports sorting, pagination, items editing and deleting
 * @param {(@link Project)[]} params.items projects array
 * @param {boolean} isLoading indicated are projects loaded
 * @param {Function} onEdit calls when user clicks 'edit' button
 * @param {Function} onDelete calls when user clicks 'delete' button
 * @param {number} page current page. Used in pagination
 * @param {Function} setPage calls when user clicks 'change page' button
 */
const ProjectsTable = ({
  items,
  isLoading,
  onEdit,
  onDelete,
  page,
  setPage,
}) => {
  const { t } = useTranslation()
  const formatDate = formatDateFactory("en")
  const fallbackString = useRef("-")
  const withFallback = useCallback((str) => str || fallbackString.current, [])
  const [anchorEl, setAnchorEl] = useState(null)
  const handleClick = (event) => setAnchorEl(event.currentTarget)
  const handleClose = () => setAnchorEl(null)
  const [rowsPerPage, setRowsPerPage] = useState(5)

  const [order, setOrder] = React.useState("asc")
  const [orderBy, setOrderBy] = React.useState("id")

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc"
    setOrder(isAsc ? "desc" : "asc")
    setOrderBy(property)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }
  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }
  const handleEdit = () => {
    onEdit(anchorEl.id)
    handleClose()
  }
  const handleDelete = () => {
    onDelete(anchorEl.id)
    handleClose()
  }

  const visibleItems = useMemo(
    () =>
      stableSort(items, getComparator(order, orderBy)).slice(
        page * rowsPerPage,
        page * rowsPerPage + rowsPerPage
      ),
    [items, page, rowsPerPage, order, orderBy]
  )

  const headCells = useMemo(
    () =>
      [
        [t("projects:table.id"), "id"],
        [t("projects:table.name"), "name"],
        [t("projects:table.status"), "status"],
        [t("projects:table.resources"), "resources"],
        [t("projects:table.price"), "price"],
        [t("projects:table.provider"), "provider"],
        [t("projects:table.complicity"), "complicity"],
        [t("projects:table.startDate"), "startDate"],
        [t("projects:table.deadLine"), "deadLine"],
        [t("projects:table.offers"), "offers"],
        ["", "actions"],
      ].map(([label, id]) => ({ label, id })),
    [t]
  )

  return (
    <>
      <Paper>
        <TableContainer>
          <Table aria-label="simple table">
            <TableHead
              headCells={headCells}
              onRequestSort={handleRequestSort}
              order={order}
              orderBy={orderBy}
            />

            <TableBody>
              {isLoading && (
                <tr>
                  <td colSpan={11}>
                    <LinearProgress />
                  </td>
                </tr>
              )}
              {visibleItems.map((row) => (
                <TableRow key={row.id}>
                  <TableCell component="th" scope="row">
                    {row.id}
                  </TableCell>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>
                    <Status {...row.status} />
                  </TableCell>
                  <TableCell>{withFallback(row.resources)}</TableCell>
                  <TableCell>{priceFormatter(row.price)}</TableCell>
                  <TableCell>{withFallback(row.provider)}</TableCell>
                  <TableCell>{percentageFormatter(row.complicity)}</TableCell>
                  <TableCell>{formatDate(row.startDate)}</TableCell>
                  <TableCell>{formatDate(row.deadLine)}</TableCell>
                  <TableCell>{row.offers}</TableCell>
                  <TableCell>
                    <IconButton
                      disabled={!row.isEditable}
                      id={row.id}
                      onClick={handleClick}
                      aria-label="more"
                      aria-haspopup="true"
                    >
                      <MoreIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={items.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleEdit}>{t("projects:edit")}</MenuItem>
        <MenuItem onClick={handleDelete}>{t("projects:delete")}</MenuItem>
      </Menu>
    </>
  )
}

ProjectsTable.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      status: PropTypes.exact({
        id: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
      }).isRequired,
      resources: PropTypes.number,
      price: PropTypes.number.isRequired,
      provider: PropTypes.string,
      complicity: PropTypes.number.isRequired,
      startDate: PropTypes.instanceOf(Date).isRequired,
      deadLine: PropTypes.instanceOf(Date).isRequired,
      offers: PropTypes.number.isRequired,
      owner: PropTypes.number.isRequired,
      initialOwner: PropTypes.number.isRequired,
      isDraft: PropTypes.bool.isRequired,
      currentUser: PropTypes.number,
      isEditable: PropTypes.bool,
    })
  ),
  isLoading: PropTypes.bool,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
}

export default ProjectsTable
