import { Button, ButtonGroup, Grid, makeStyles } from "@material-ui/core"
import React from "react"
import PropTypes from "prop-types"

const useStyles = makeStyles((theme) => ({
  projectTypeButtons: {
    display: "flex",
    justifyContent: "flex-end",
  },
  buttons: {
    margin: theme.spacing(2),
  },
}))

/**
 * Provides project type selection
 * @param {Status[]} params.statuses available statuses
 * @param {Function} params.onChange fires when user changes current status
 * @param {string} params.currentStatus currently displayed status
 */
const ProjectTypeSelectors = ({ statuses, onChange, currentStatus }) => {
  const classes = useStyles()
  const onFilterSelect = (e) => onChange(e.currentTarget.id)

  return (
    <Grid container spacing={4} className={classes.projectTypeButtons}>
      <Grid item className={classes.buttons}>
        <ButtonGroup color="primary" variant="outlined">
          {statuses.map(({ id, text }) => (
            <Button
              id={id}
              key={id}
              variant={currentStatus === id ? "contained" : undefined}
              onClick={onFilterSelect}
            >
              {text}
            </Button>
          ))}
        </ButtonGroup>
      </Grid>
    </Grid>
  )
}

ProjectTypeSelectors.propTypes = {
  statuses: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    })
  ).isRequired,
  onChange: PropTypes.func.isRequired,
  currentStatus: PropTypes.string.isRequired,
}

export default ProjectTypeSelectors
