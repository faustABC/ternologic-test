import React from "react"
import { fireEvent, render } from "@testing-library/react"
import ProjectList from "./App"
import { rootReducer } from "../../store"
import { PrivateRoute, StoreProvider } from "common"
import { Router } from "@reach/router"
import { ROUTING } from "configuration"
import { act } from "react-dom/test-utils"
import { tokenSerializer } from "../../App"
import { generateAccessToken, initialData } from "fetch/data"

describe("ProjectList", () => {
  it("Private router redirects user after reload if there are valid credentials", async () => {
    await act(async () => {
      tokenSerializer.serializeToken(generateAccessToken(initialData.users[0]))

      const { findByText } = render(
        <StoreProvider rootReducer={rootReducer}>
          <Router>
            <PrivateRoute
              default
              fallbackUri={ROUTING.AUTH_REQUIRED}
              component={ProjectList}
            />
          </Router>
        </StoreProvider>
      )

      await findByText(/create new project/i)
    })
  })
  it("Private router do NOT redirect user after reload if there are INvalid credentials", async () => {
    await act(async () => {
      tokenSerializer.serializeToken(generateAccessToken(initialData.users[0]))

      const { queryByText } = render(
        <StoreProvider rootReducer={rootReducer}>
          <Router>
            <PrivateRoute
              default
              fallbackUri={ROUTING.AUTH_REQUIRED}
              component={ProjectList}
            />
          </Router>
        </StoreProvider>
      )

      expect(queryByText(/create new project/i)).toBeNull()
    })
  })
  it("Search by any field", async () => {
    await act(async () => {
      tokenSerializer.serializeToken(generateAccessToken(initialData.users[0]))
      console.error = () => undefined

      const { findByPlaceholderText, findAllByText } = render(
        <StoreProvider rootReducer={rootReducer}>
          <Router>
            <PrivateRoute
              default
              fallbackUri={ROUTING.AUTH_REQUIRED}
              component={ProjectList}
            />
          </Router>
        </StoreProvider>
      )

      fireEvent.input(await findByPlaceholderText(/search by any property/i), {
        target: { value: "google" },
      })

      expect((await findAllByText(/google/i)).length).toBe(1)
    })
  })
  it("Filter by project status", async () => {
    await act(async () => {
      const { findByText, findAllByText } = render(
        <StoreProvider rootReducer={rootReducer}>
          <Router>
            <PrivateRoute
              default
              fallbackUri={ROUTING.AUTH_REQUIRED}
              component={ProjectList}
            />
          </Router>
        </StoreProvider>
      )

      fireEvent.click(await findByText("My"))

      expect((await findAllByText("Education System")).length).toBe(1)
      expect((await findAllByText("Information System")).length).toBe(1)
      expect((await findAllByText("Accounting System")).length).toBe(1)
    })
  })
  it("Filter by project status", async () => {
    await act(async () => {
      const { findByText, findAllByText } = render(
        <StoreProvider rootReducer={rootReducer}>
          <Router>
            <PrivateRoute
              default
              fallbackUri={ROUTING.AUTH_REQUIRED}
              component={ProjectList}
            />
          </Router>
        </StoreProvider>
      )

      fireEvent.click(await findByText("My"))

      expect((await findAllByText("Education System")).length).toBe(1)
      expect((await findAllByText("Information System")).length).toBe(1)
      expect((await findAllByText("Accounting System")).length).toBe(1)
    })
  })
})
