import {
  createReducerWithAsyncHandling,
  combineReducers,
} from "common/state/utils"
import produce from "immer"
import { AVAILABLE_STATUSES } from "configuration"
import {
  ADD_PROJECT,
  CHANGE_PROJECT,
  DELETE_PROJECT,
  GET_PROJECTS,
} from "./actions"

export const allProjectsInitialState = {
  projects: [],
  isLoading: false,
  error: null,
}

export const statusInitialState = AVAILABLE_STATUSES.ALL
export const formVisibilityInitialState = false

const allProjectsReducer = (state = allProjectsInitialState, action) =>
  createReducerWithAsyncHandling(GET_PROJECTS, {
    onStart: (state) => ({ ...state, projects: [] }),
    onSuccess: produce((state, action) => {
      state.projects = action.projects
    }),
  })(state, action) ||
  createReducerWithAsyncHandling(ADD_PROJECT, {
    onSuccess: produce((state, action) => {
      state.projects.push(action.project)
    }),
  })(state, action) ||
  createReducerWithAsyncHandling(CHANGE_PROJECT, {
    onSuccess: produce((state, action) => {
      const projectIndex = state.projects.findIndex(
        (project) => project.id === action.project.id
      )
      state.projects[projectIndex] = action.project
    }),
  })(state, action) ||
  createReducerWithAsyncHandling(DELETE_PROJECT, {
    onSuccess: produce((state, action) => {
      const projectIndex = state.projects.findIndex(
        (project) => project.id === action.projectId
      )
      state.projects = [
        ...state.projects.slice(0, projectIndex),
        ...state.projects.slice(projectIndex + 1, state.projects.length),
      ]
    }),
  })(state, action) ||
  state

const currentStatusReducer = (state = statusInitialState, action) =>
  action.status || state

const formVisibilityReducer = (state = formVisibilityInitialState, action) =>
  action.isVisible !== undefined ? action.isVisible : state

/**
 * all - all downloaded projects in the app
 * currentStatus - current project status to filter for
 * isFormVisible - indicated if editing/creation form is visible
 */
export const projectsReducer = combineReducers({
  all: allProjectsReducer,
  currentStatus: currentStatusReducer,
  isFormVisible: formVisibilityReducer,
})
