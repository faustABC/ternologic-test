import { useDispatch, useSelector } from "common"
import { getStatuses } from "common/state/actions"
import { useCallback } from "react"
import { apiClient } from "../../../App"
import { parseProject, parseProjects } from "../utils"
import {
  addProject,
  changeProject,
  deleteProject,
  getProjects,
  setFormVisibility,
} from "./actions"

export const useProjectsEffects = () => {
  const dispatch = useDispatch()
  const { currentStatus } = useSelector((state) => state.projects)
  const { id: currentUser } = useSelector((state) => state.users.current)
  const { isFormVisible } = useSelector((state) => state.projects)

  const getProjectIdToEditingMapSelector = (state) =>
    state.projects.all.projects.reduce(
      (acc, proj) => ({
        ...acc,
        [proj.id]: proj.owner === state.users.current.id,
      }),
      {}
    )

  const getProjectsAsync = useCallback(
    async (filter) => {
      try {
        dispatch(getProjects.START())
        const searchParams = { status: currentStatus, currentUser }
        if (filter) {
          searchParams.filter = filter
        }
        const projects = await apiClient.get("projects", {
          searchParams,
        })
        const parsedProjects = parseProjects(projects)
        dispatch(getProjects.SUCCESS(parsedProjects))
      } catch (e) {
        dispatch(getProjects.ERROR(e.message))
      }
    },
    [dispatch, apiClient, currentStatus, currentUser]
  )

  const addProjectAsync = useCallback(
    async (newProject) => {
      try {
        dispatch(addProject.START())
        const project = await apiClient.post("projects", {
          body: { ...newProject, currentUser },
        })
        const parsedProject = parseProject(project)
        dispatch(addProject.SUCCESS(parsedProject))
      } catch (e) {
        dispatch(addProject.ERROR(e.message))
      }
    },
    [dispatch, apiClient, currentStatus, currentUser]
  )

  const editProjectAsync = useCallback(
    async (editedProject) => {
      try {
        dispatch(changeProject.START())
        const project = await apiClient.put(`projects/${editedProject.id}`, {
          body: { ...editedProject, currentUser },
        })
        const parsedProject = parseProject(project)
        dispatch(changeProject.SUCCESS(parsedProject))
      } catch (e) {
        dispatch(changeProject.ERROR(e.message))
      }
    },
    [dispatch, apiClient, currentStatus, currentUser]
  )

  const deleteProjectAsync = useCallback(
    async (projectId) => {
      try {
        dispatch(deleteProject.START())
        await apiClient.delete(`projects/${projectId}`)
        dispatch(deleteProject.SUCCESS(projectId))
      } catch (e) {
        dispatch(deleteProject.ERROR(e.message))
      }
    },
    [dispatch, apiClient, currentStatus, currentUser]
  )

  const getStatusesAsync = useCallback(async () => {
    const statuses = await apiClient.get("statuses")
    dispatch(getStatuses.SUCCESS(statuses))
  }, [dispatch, apiClient])

  const toggleFormVisibility = useCallback(
    () => dispatch(setFormVisibility(!isFormVisible)),
    [dispatch, isFormVisible, setFormVisibility]
  )

  return {
    getProjectsAsync,
    addProjectAsync,
    toggleFormVisibility,
    getStatusesAsync,
    editProjectAsync,
    deleteProjectAsync,
    getProjectIdToEditingMapSelector,
  }
}
