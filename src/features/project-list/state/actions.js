import {
  createActionCreatorsWithAsyncHandling,
  createActionNamesWithAsyncHandling,
} from "common/state/utils"

export const GET_PROJECTS = createActionNamesWithAsyncHandling("GET_PROJECTS")
export const ADD_PROJECT = createActionNamesWithAsyncHandling("ADD_PROJECT")
export const CHANGE_PROJECT = createActionNamesWithAsyncHandling(
  "CHANGE_PROJECT"
)
export const DELETE_PROJECT = createActionNamesWithAsyncHandling(
  "DELETE_PROJECT"
)
export const SET_CURRENT_STATUS = "SET_CURRENT_STATUS"
export const SET_FORM_VISIBILITY = "SET_FORM_VISIBILITY"

export const getProjects = createActionCreatorsWithAsyncHandling(
  GET_PROJECTS,
  (projects) => ({
    type: GET_PROJECTS.SUCCESS,
    projects,
  })
)

export const addProject = createActionCreatorsWithAsyncHandling(
  ADD_PROJECT,
  (project) => ({
    type: ADD_PROJECT.SUCCESS,
    project,
  })
)

export const changeProject = createActionCreatorsWithAsyncHandling(
  CHANGE_PROJECT,
  (project) => ({
    type: CHANGE_PROJECT.SUCCESS,
    project,
  })
)

export const deleteProject = createActionCreatorsWithAsyncHandling(
  DELETE_PROJECT,
  (projectId) => ({
    type: DELETE_PROJECT.SUCCESS,
    projectId,
  })
)

export const setCurrentStatus = (status) => ({
  type: SET_CURRENT_STATUS,
  status,
})

export const setFormVisibility = (isVisible) => ({
  type: SET_FORM_VISIBILITY,
  isVisible,
})
