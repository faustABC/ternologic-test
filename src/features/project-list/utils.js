export const priceFormatter = (price) => `$${price}k`

export const percentageFormatter = (percent) => `${Math.floor(percent * 100)}%`

export const parseProject = ({
  startDate,
  deadLine,
  owner,
  offers,
  price,
  ...rest
}) => ({
  ...rest,
  startDate: new Date(startDate),
  deadLine: new Date(deadLine),
  owner: Number.parseInt(owner, 10),
  offers: Number.parseInt(offers, 10),
  price: Number(price),
})

export const parseProjects = (projects) => projects.map(parseProject)

export const formatDateToInput = (date) => date.toISOString().slice(0, 10)

export const formProjectToDto = (formProject) => ({
  ...formProject,
  complicity: Math.floor(formProject.complicity) / 100,
  startDate: new Date(formProject.startDate),
  deadLine: new Date(formProject.deadLine),
})

export const dtoToFormProject = (projectDto) => ({
  ...projectDto,
  status: projectDto.status.id,
  complicity: projectDto.complicity * 100,
  startDate: formatDateToInput(projectDto.startDate),
  deadLine: formatDateToInput(projectDto.deadLine),
})

export const descendingComparator = (a, b, orderBy) =>
  b[orderBy] < a[orderBy] ? -1 : b[orderBy] > a[orderBy] ? 1 : 0

export const getComparator = (order, orderBy) =>
  order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy)

export const stableSort = (array, comparator) => {
  const stabilizedThis = array.map((el, index) => [el, index])
  stabilizedThis.sort((a, b) => {
    const order = comparator(
      formatProjectForSorting(a[0]),
      formatProjectForSorting(b[0])
    )
    if (order !== 0) return order
    return a[1] - b[1]
  })
  return stabilizedThis.map((el) => el[0])
}

export const formatProjectForSorting = (project) => ({
  ...project,
  status: project.status.text,
})
