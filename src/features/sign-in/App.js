import { makeStyles, Paper, Typography } from "@material-ui/core"
import React, { useEffect } from "react"
import { Button, PasswordInput, TextInput, Layout, useSelector } from "common"
import { useNavigate } from "@reach/router"
import { useForm } from "hooks"
import { ROUTING } from "configuration"
import { useTranslation } from "react-i18next"
import { useEffects } from "./effects"

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    gap: theme.spacing(2),
  },
  itemFullWidth: {
    gridColumn: "1 / 3",
  },
  paper: {
    width: "min(400px, 100%)",
    padding: theme.spacing(2),
    height: "fit-content",
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    gap: theme.spacing(2),
  },
}))

/**
 * Displays sign in page with email and password inputs
 */
function App() {
  const { t } = useTranslation()
  const { current, isLoading, error } = useSelector((state) => state.users)
  const classes = useStyles()
  const navigate = useNavigate()
  const { signIn } = useEffects()
  const { inputProps, data, isAllFilled, isNoErrors } = useForm({
    email: "",
    password: "",
  })

  const onSubmit = (e) => {
    e.preventDefault()
    signIn(data.values.email, data.values.password)
  }

  useEffect(() => {
    current && navigate(ROUTING.ROOT)
  }, [current])

  return (
    <Layout>
      <form className={classes.root} onSubmit={onSubmit}>
        <Paper variant="outlined" className={classes.paper}>
          <Typography variant="overline" className={classes.itemFullWidth}>
            {t("auth:signIn")}
          </Typography>
          <TextInput
            {...inputProps.email}
            autoComplete="email"
            label={t("auth:email")}
            className={classes.itemFullWidth}
          />
          <PasswordInput
            {...inputProps.password}
            autoComplete="password"
            label={t("auth:password")}
            className={classes.itemFullWidth}
          />
          <Button
            type="submit"
            color="primary"
            variant="contained"
            style={{ gridColumnStart: 2 }}
            disabled={!isAllFilled || !isNoErrors || isLoading}
            isLoading={isLoading}
            data-testid="submit"
          >
            {t("auth:signIn")}
          </Button>
        </Paper>
        {error && (
          <Paper
            variant="outlined"
            className={classes.paper}
            style={{ gridTemplateColumns: "1fr" }}
          >
            <Typography color="error" variant="caption">
              {error}
            </Typography>
          </Paper>
        )}
      </form>
    </Layout>
  )
}

export default App
