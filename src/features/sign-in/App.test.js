import React from "react"
import { fireEvent, render } from "@testing-library/react"
import SignIn from "./App"
import { rootReducer } from "../../store"
import { StoreProvider } from "common"
import { Router } from "@reach/router"
import { act } from "react-dom/test-utils"

describe("SignIn", () => {
  it("All text is localized", async () => {
    act(() => {
      const { getByText, getAllByText } = render(
        <StoreProvider rootReducer={rootReducer}>
          <Router>
            <SignIn default />
          </Router>
        </StoreProvider>
      )

      expect(getByText(/email/i)).toBeInTheDocument()
      expect(getByText(/password/i)).toBeInTheDocument()
      expect(getAllByText(/sign in/i).length).toBe(2)
    })
  })
  it("Failed to auth", async () => {
    await act(async () => {
      const { findByText, getByLabelText } = render(
        <StoreProvider rootReducer={rootReducer}>
          <Router>
            <SignIn default />
          </Router>
        </StoreProvider>
      )
      fireEvent.input(getByLabelText(/email/i), {
        target: { value: "root@root.com" },
      })
      fireEvent.input(getByLabelText("Password"), {
        target: { value: "incorrect value" },
      })
      fireEvent.submit(getByLabelText("Password"))
      expect(await findByText(/(not found)|(invalid url)/i)).toBeInTheDocument()
    })
  })
  it("Successfully auth", async () => {
    await act(async () => {
      const { getByLabelText, getByTestId, queryByText } = render(
        <StoreProvider rootReducer={rootReducer}>
          <Router>
            <SignIn default />
          </Router>
        </StoreProvider>
      )
      fireEvent.input(getByLabelText(/email/i), {
        target: { value: "root@root.com" },
      })
      fireEvent.input(getByLabelText("Password"), {
        target: { value: "root" },
      })
      fireEvent.submit(getByTestId("submit"))
      expect(await queryByText(/not found/i)).toBeNull()
    })
  })
})
