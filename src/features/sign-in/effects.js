import { actions, useDispatch } from "common"
import { apiClient, tokenSerializer } from "../../App"

export const useEffects = () => {
  const dispatch = useDispatch()

  const signIn = async (email, password) => {
    try {
      dispatch(actions.signIn.START())
      const { user, accessToken } = await apiClient.post("sign-in", {
        body: { email, password },
      })
      tokenSerializer.serializeToken(accessToken)
      dispatch(actions.signIn.SUCCESS(user))
    } catch (e) {
      dispatch(actions.signIn.ERROR(e.message))
    }
  }

  return { signIn }
}
