import { generateVId, generateId } from "utils"

const statuses = {
  inProgress: {
    id: "inProgress",
    text: "In Progress",
    color: "#FF0000",
  },
  design: {
    id: "design",
    text: "Design",
    color: "#00FF00",
  },
  inReview: {
    id: "inReview",
    text: "In Review",
    color: "#0000FF",
  },
}

const users = [
  {
    id: generateId(),
    firstName: "Root",
    lastName: "Root",
    email: "root@root.com",
    password: "root",
  },
  {
    id: generateId(),
    firstName: "Root 1",
    lastName: "Root 1",
    email: "root@root.com 1",
    password: "root 1",
  },
]

let projects = [
  {
    id: generateVId(),
    owner: users[0].id,
    initialOwner: users[1].id,
    name: "Information System",
    status: statuses.design,
    resources: 10,
    price: 550,
    provider: "Netflix",
    complicity: 0.5,
    startDate: new Date(),
    deadLine: new Date(),
    offers: 2,
    isDraft: true,
  },
  {
    id: generateVId(),
    owner: users[0].id,
    initialOwner: users[0].id,
    name: "Accounting System",
    status: statuses.inReview,
    resources: 2,
    price: 100,
    provider: "Amazon",
    complicity: 0.1,
    startDate: new Date(),
    deadLine: new Date(),
    offers: 1,
    isDraft: false,
  },
  {
    id: generateVId(),
    owner: users[1].id,
    initialOwner: users[1].id,
    name: "Banking Software",
    status: statuses.inReview,
    price: 40,
    complicity: 0.2,
    startDate: new Date(),
    deadLine: new Date(),
    offers: 18,
    isDraft: false,
    provider: "Google",
    resources: null,
  },
  {
    id: generateVId(),
    owner: users[0].id,
    initialOwner: users[1].id,
    name: "Education System",
    status: statuses.inProgress,
    resources: 1,
    price: 55,
    provider: "AirBnb",
    complicity: 0.9,
    startDate: new Date(),
    deadLine: new Date(),
    offers: 20,
    isDraft: false,
  },
]

export const AVAILABLE_STATUSES = {
  ALL: "ALL",
  MY: "MY",
  ACQUIRED: "ACQUIRED",
  DRAFTED: "DRAFTED",
}

export const initialData = {
  projects,
  users,
  statuses,
}

const combineFilters = (...filters) => (item) =>
  filters.reduce((isValid, filter) => isValid && filter(item), true)

const voidFilter = () => true

const formatProject = (project) => ({
  ...project,
  status: project.status.text,
})

export const getProjects = ({ status, currentUser, filter: filterText }) => {
  const parsedCurrentUser = Number.parseInt(currentUser, 10)

  const statusFilter = {
    [AVAILABLE_STATUSES.ACQUIRED]: (project) =>
      project.owner === parsedCurrentUser &&
      project.initialOwner !== parsedCurrentUser,
    [AVAILABLE_STATUSES.MY]: (project) => project.owner === parsedCurrentUser,
    [AVAILABLE_STATUSES.DRAFTED]: (project) => project.isDraft,
    [AVAILABLE_STATUSES.ALL]: () => true,
  }[status]

  const textFilter = filterText
    ? (project) =>
        !!Object.values(formatProject(project)).find((value) =>
          new RegExp(filterText, "i").test(value)
        )
    : voidFilter

  return initialData.projects.filter(combineFilters(statusFilter, textFilter))
}

export const addProject = ({
  id = generateVId(),
  currentUser,
  name,
  status,
  price = 0,
  complicity = 0,
  startDate,
  deadLine,
  offers = 0,
  provider,
}) => {
  const newProject = {
    owner: currentUser,
    initialOwner: currentUser,
    startDate: new Date(startDate),
    deadLine: new Date(deadLine),
    isDraft: false,
    id,
    price,
    complicity,
    name,
    status: statuses[status],
    offers,
    provider,
  }
  initialData.projects.push(newProject)
  return newProject
}

export const editProject = (id, project) => {
  const projectIndex = initialData.projects.findIndex((p) => id === p.id)
  const editedProject = {
    ...initialData.projects[projectIndex],
    ...project,
    owner: project.currentUser,
    initialOwner: project.currentUser,
    startDate: new Date(project.startDate),
    deadLine: new Date(project.deadLine),
    status: statuses[project.status],
  }

  initialData.projects[projectIndex] = editedProject
  return editedProject
}

export const getUser = (email, password) =>
  initialData.users.find(
    (user) => user.email === email && user.password === password
  )

export const addUser = (email, password, firstName, lastName) => {
  const user = {
    id: generateId(),
    email,
    password,
    firstName,
    lastName,
  }

  users.push(user)
  return user
}

export const getStatuses = () => Object.values(statuses)

export const deleteProject = (id) => {
  const projectIndex = initialData.projects.findIndex((p) => p.id === id)
  initialData.projects = [
    ...initialData.projects.slice(0, projectIndex),
    ...initialData.projects.slice(
      projectIndex + 1,
      initialData.projects.length
    ),
  ]
}

export const generateAccessToken = (user) => JSON.stringify(user)
export const getUserFromAccessToken = (token) => JSON.parse(token)
