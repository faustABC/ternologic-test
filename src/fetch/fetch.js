import {
  getProjects,
  getUser,
  addUser,
  addProject,
  getStatuses,
  editProject,
  deleteProject,
  generateAccessToken,
  getUserFromAccessToken,
} from "./data"

/**
 * Mocked fetch. Used for mocking database calls
 * @param {string} path path where is the destination of the request
 * @param {object} options options of fetch
 */
export const fetch = (path, options) => {
  const url = new URL(path)
  const body = options.body && JSON.parse(options.body)
  const searchParams = Object.fromEntries([...url.searchParams.entries()])

  let responseBody = {}
  let responseStatus = 200
  let responseStatusText = "Ok"

  if (url.pathname === "/api/auth" && options.method === "POST") {
    try {
      const user = getUserFromAccessToken(body.accessToken)
      if (user) {
        responseBody = { user }
        responseStatus = 200
      } else {
        responseStatus = 401
        responseStatusText = "Unauthorized"
      }
    } catch (e) {
      responseStatus = 401
      responseStatusText = "Unauthorized"
    }
  } else if (url.pathname === "/api/sign-in" && options.method === "POST") {
    const user = getUser(body.email, body.password)
    if (user) {
      const accessToken = generateAccessToken(user)
      responseBody = { user, accessToken }
      responseStatus = 200
    } else {
      responseStatus = 404
      responseStatusText = "Not Found"
    }
  } else if (url.pathname === "/api/sign-up" && options.method === "POST") {
    const user = addUser(
      body.email,
      body.password,
      body.firstName,
      body.lastName
    )
    const accessToken = generateAccessToken(user)
    responseBody = { user, accessToken }
    responseStatus = 201
  } else if (url.pathname === "/api/projects" && options.method === "GET") {
    responseBody = getProjects({
      status: searchParams.status,
      currentUser: searchParams.currentUser,
      filter: searchParams.filter,
    })
    responseStatus = 200
  } else if (url.pathname === "/api/projects" && options.method === "POST") {
    responseBody = addProject(body)
    responseStatus = 200
  } else if (url.pathname === "/api/statuses" && options.method === "GET") {
    responseBody = getStatuses()
    responseStatus = 200
  } else if (
    /\/api\/projects\/.+/.test(url.pathname) &&
    options.method === "PUT"
  ) {
    const id = url.pathname.split("/").pop()
    responseBody = editProject(id, body)
    responseStatus = 200
  } else if (
    /\/api\/projects\/.+/.test(url.pathname) &&
    options.method === "DELETE"
  ) {
    const id = url.pathname.split("/").pop()
    deleteProject(id)
  }

  const finalResponse = new Response(JSON.stringify(responseBody), {
    status: responseStatus,
    statusText: responseStatusText,
  })

  return Promise.resolve(finalResponse)
}
