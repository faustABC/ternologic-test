import { useEffect, useState } from "react"

/**
 * Accepts value and returns useState like object [debouncedValue, setDebouncedValue]
 * debouncedValue will be set to value if value will not be changed through delay milliseconds
 * @param {any} value any value need to be debounced
 * @param {number} delay amount of milliseconds to debounce with
 */
export const useDebouncedValue = (value, delay) => {
  const [debouncedValue, setDebouncedValue] = useState(value)

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value)
    }, delay)

    return () => clearTimeout(handler)
  }, [value, delay])

  return [debouncedValue, setDebouncedValue]
}
