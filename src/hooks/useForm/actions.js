export const SET_FORM_VALUE = "SET_FORM_VALUE"
export const SET_FORM_ERRORS = "SET_FORM_ERRORS"
export const SET_FORM_BLURRED = "SET_FORM_BLURRED"
export const SET_FORM_TOUCHED = "SET_FORM_TOUCHED"

export const setFormValue = (property, value) => ({
  type: SET_FORM_VALUE,
  property,
  value,
})

export const setFormErrors = (errors) => ({
  type: SET_FORM_ERRORS,
  errors,
})

export const setFormBlurred = (property) => ({
  type: SET_FORM_BLURRED,
  property,
})

export const setFormTouched = (property) => ({
  type: SET_FORM_TOUCHED,
  property,
})
