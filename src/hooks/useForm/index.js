import { useEffect, useMemo, useReducer } from "react"
import { arrayToObject } from "utils"
import {
  setFormBlurred,
  setFormErrors,
  setFormTouched,
  setFormValue,
} from "./actions"
import { formReducer } from "./reducer"

/**
 *
 * @param {object} propertiesMap object with properties - input names,
 * and values - default form values
 * @param {Function} validation validation callback
 */
export const useForm = (propertiesMap, validation) => {
  const properties = Object.keys(propertiesMap)

  const values = propertiesMap
  const errors = arrayToObject(properties, undefined, () => new Array())
  const blurred = arrayToObject(properties, false)
  const touched = arrayToObject(properties, false)

  const [formStore, dispatch] = useReducer(formReducer, {
    values,
    errors,
    blurred,
    touched,
  })

  const setProperty = (property, value) =>
    dispatch(setFormValue(property, value))

  const onBlur = (e) => dispatch(setFormBlurred(e.target.name))

  const onChange = (e) => {
    dispatch(setFormValue(e.target.name, e.target.value))
    dispatch(setFormTouched(e.target.name))
  }

  useEffect(() => {
    if (!validation) return
    const newErrors = arrayToObject(properties, undefined, () => new Array())
    dispatch(setFormErrors(validation({ ...formStore, newErrors })))
  }, [...Object.values(formStore.values), ...Object.values(formStore.blurred)])

  const inputProps = properties.reduce(
    (acc, prop) => ({
      ...acc,
      [prop]: {
        name: prop,
        id: prop,
        onChange,
        onBlur,
        value: formStore.values[prop],
        errors: formStore.errors[prop],
      },
    }),
    {}
  )

  const isAllFilled = useMemo(
    () =>
      Object.values(formStore.values).reduce(
        (isFilled, value) => isFilled && !!value,
        true
      ),
    [formStore.values]
  )

  const isNoErrors = useMemo(
    () =>
      Object.values(formStore.errors).reduce(
        (noErrors, errors) => noErrors && !errors.length,
        true
      ),
    [formStore.errors]
  )

  return {
    data: formStore,
    inputProps,
    setProperty,
    onBlur,
    onChange,
    isAllFilled,
    isNoErrors,
  }
}
