import produce from "immer"
import {
  SET_FORM_BLURRED,
  SET_FORM_ERRORS,
  SET_FORM_TOUCHED,
  SET_FORM_VALUE,
} from "./actions"

export const formReducer = (state, action) => {
  switch (action.type) {
    case SET_FORM_VALUE:
      return produce(state, (draft) => {
        draft.values[action.property] = action.value
      })

    case SET_FORM_ERRORS:
      return {
        ...state,
        errors: action.errors,
      }

    case SET_FORM_BLURRED:
      return produce(state, (draft) => {
        draft.blurred[action.property] = true
      })

    case SET_FORM_TOUCHED:
      return produce(state, (draft) => {
        draft.touched[action.property] = true
      })

    default:
      return state
  }
}
