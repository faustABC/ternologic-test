import React from "react"
import { Router, Redirect } from "@reach/router"
import { SignUp, SignIn, AuthSuggestion, ProjectList } from "./features"
import CssBaseline from "@material-ui/core/CssBaseline"
import { createMuiTheme, ThemeProvider } from "@material-ui/core"
import { lightTheme } from "styles"
import { StoreProvider, PrivateRoute } from "common"
import { rootReducer } from "./store"
import { ACCESS_TOKEN_NAME, ROUTING } from "configuration"
import { TokenSerializer } from "utils"
import { ApiClient } from "api"
import fetch from "fetch"

export const apiClient = new ApiClient(
  process.env.REACT_APP_BASE_API_URL || "http://localhost:3000/api",
  fetch
)

export const tokenSerializer = new TokenSerializer(
  localStorage,
  ACCESS_TOKEN_NAME
)

/**
 * Main component in the application.
 * Contains configuration, including routing, Material ui theming, and store
 */
function App() {
  return (
    <ThemeProvider theme={createMuiTheme(lightTheme)}>
      <StoreProvider rootReducer={rootReducer}>
        <Router>
          <SignUp path={ROUTING.SIGN_UP} />
          <SignIn path={ROUTING.SIGN_IN} />
          <PrivateRoute
            path={ROUTING.ROOT}
            component={ProjectList}
            fallbackUri={ROUTING.AUTH_REQUIRED}
          />
          <AuthSuggestion
            signInLink={ROUTING.SIGN_IN}
            signUpLink={ROUTING.SIGN_UP}
            path={ROUTING.AUTH_REQUIRED}
          />
          <Redirect from="*" noThrow to={ROUTING.AUTH_REQUIRED} />
        </Router>
        <CssBaseline />
      </StoreProvider>
    </ThemeProvider>
  )
}

export default App
