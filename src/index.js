import "react-app-polyfill/ie11"
import "react-app-polyfill/stable"
import "jss-plugin-vendor-prefixer"
import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import { initI18n } from "utils"
import * as resources from "locale"

/** Inits internationalization */
initI18n(resources)

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
)
