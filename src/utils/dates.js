export const formatDateFactory = (locale) => (date) =>
  new Intl.DateTimeFormat(locale).format(date)
