/**
 * @class Handles token in store
 */
export class TokenSerializer {
  /**
   * @param {Storage} storage storage like object to handle token
   * @param {string} tokenName name of the token
   */
  constructor(storage, tokenName) {
    this.storage = storage
    this.tokenName = tokenName
  }

  serializeToken(token) {
    this.storage.setItem(this.tokenName, token)
  }

  deserializeToken() {
    return this.storage.getItem(this.tokenName)
  }
}
