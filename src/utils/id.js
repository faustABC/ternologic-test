export const generateId = () => Math.floor(Math.random() * 10 ** 8)

export const generateVId = () =>
  Array(16)
    .fill(null)
    .map(() => Math.floor(10 * Math.random()))
    .join("")
