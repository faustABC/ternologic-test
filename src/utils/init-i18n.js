import i18n from "i18next"
import { initReactI18next } from "react-i18next"

export const initI18n = (resources) => {
  i18n.use(initReactI18next).init({
    resources,
    lng: "en",
    fallbackLng: "en",
    interpolation: {
      escapeValue: false,
    },
    debug: process.env.NODE_ENV === "development",
  })
}
