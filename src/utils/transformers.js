export const arrayToObject = (properties, defaultValue, createDefaultValue) =>
  properties.reduce((acc, prop) => {
    const value =
      createDefaultValue !== undefined ? createDefaultValue() : defaultValue
    return { ...acc, [prop]: value }
  }, {})
