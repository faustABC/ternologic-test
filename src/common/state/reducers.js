import produce from "immer"
import { GET_STATUSES, GET_USER, LOG_OUT, SIGN_IN, SIGN_UP } from "./actions"
import { createReducerWithAsyncHandling } from "./utils"

const initialUserState = {
  current: null,
  isLoading: false,
  error: null,
}

const initialStatusState = {
  statuses: [],
  isLoading: false,
  error: null,
}

export const logOutReducer = (state, action) => {
  switch (action.type) {
    case LOG_OUT:
      return {
        ...state,
        current: null,
      }
  }
}

/**
 * Users in the application
*/
export const userReducer = (state = initialUserState, action) =>
  createReducerWithAsyncHandling(SIGN_IN, {
    onSuccess: produce((state, action) => {
      state.current = action.user
    }),
  })(state, action) ||
  createReducerWithAsyncHandling(SIGN_UP, {
    onSuccess: produce((state, action) => {
      state.current = action.user
    }),
  })(state, action) ||
  createReducerWithAsyncHandling(GET_USER, {
    onSuccess: produce((state, action) => {
      state.current = action.user
    }),
  })(state, action) ||
  logOutReducer(state, action) ||
  state

/**
 * All available statuses
 */
export const statusReducer = (state = initialStatusState, action) =>
  createReducerWithAsyncHandling(GET_STATUSES, {
    onSuccess: produce((state, action) => {
      state.statuses = action.statuses
    }),
  })(state, action) || state
