/**
 * @typedef {object} Actions
 * @property {string} START
 * @property {string} SUCCESS
 * @property {string} ERROR
 */

/**
 * Creates 3 actions with same root name:
 * START, SUCCESS, ERROR.
 * Used with async handling
 * @param {string} rootName name of actions, namespace
 * @returns {@link Actions}
 */
export const createActionNamesWithAsyncHandling = (rootName) => ({
  START: `${rootName}/START`,
  SUCCESS: `${rootName}/SUCCESS`,
  ERROR: `${rootName}/ERROR`,
})

/**
 * Creates action creators for async action templates
 * @param {@link Actions} actionNames names from createActionNamesWithAsyncHandling
 * @param {Function} onSuccess function that will be executed when action will be SUCCESS
 */
export const createActionCreatorsWithAsyncHandling = (
  actionNames,
  onSuccess
) => ({
  START: () => ({ type: actionNames.START }),
  SUCCESS: onSuccess,
  ERROR: (error) => ({ type: actionNames.ERROR, error }),
})

/**
 *
 * @param {@link Actions} actionNames names from createActionNamesWithAsyncHandling
 * @param {{onSuccess: Function, onStart: Function}} [options] general reducer
 * for async event templates
 */
export const createReducerWithAsyncHandling = (
  actionNames,
  { onSuccess = () => ({}), onStart = () => ({}) }
) => (state, action) => {
  switch (action.type) {
    case actionNames.START:
      return {
        ...state,
        ...onStart(state, action),
        isLoading: true,
        error: null,
      }

    case actionNames.SUCCESS:
      return {
        ...state,
        ...onSuccess(state, action),
        isLoading: false,
        error: null,
      }

    case actionNames.ERROR:
      return { ...state, isLoading: false, error: action.error }

    default:
      return null
  }
}

/**
 * Creates one reducer from passed reducers map
 * @param {object} reducersMap map of reducers to be merged
 */
export const combineReducers = (reducersMap) => (state, action) =>
  Object.entries(reducersMap).reduce(
    (acc, [name, reducer]) => ({
      ...acc,
      [name]: reducer(state && state[name], action),
    }),
    {}
  )
