import {
  createActionCreatorsWithAsyncHandling,
  createActionNamesWithAsyncHandling,
} from "./utils"

export const SIGN_IN = createActionNamesWithAsyncHandling("SIGN_IN")
export const SIGN_UP = createActionNamesWithAsyncHandling("SIGN_UP")
export const LOG_OUT = createActionNamesWithAsyncHandling("LOG_OUT")
export const GET_USER = createActionNamesWithAsyncHandling("GET_USER")
export const GET_STATUSES = createActionNamesWithAsyncHandling("GET_STATUSES")

export const signIn = createActionCreatorsWithAsyncHandling(
  SIGN_IN,
  (user) => ({
    type: SIGN_IN.SUCCESS,
    user,
  })
)

export const signUp = createActionCreatorsWithAsyncHandling(
  SIGN_UP,
  (user) => ({
    type: SIGN_UP.SUCCESS,
    user,
  })
)

export const getUser = createActionCreatorsWithAsyncHandling(
  GET_USER,
  (user) => ({
    type: GET_USER.SUCCESS,
    user,
  })
)

export const getStatuses = createActionCreatorsWithAsyncHandling(
  GET_STATUSES,
  (statuses) => ({
    type: GET_STATUSES.SUCCESS,
    statuses,
  })
)

export const logOut = () => ({
  type: LOG_OUT,
})
