import { Redirect } from "@reach/router"
import React, { useEffect } from "react"
import { useDispatch, useSelector } from "./StoreProvider"
import PropTypes from "prop-types"
import { actions } from "common"
import { apiClient, tokenSerializer } from "../../../App"

/**
 * PrivateRoute is used to limit user access to restricted resource 
 * @param {React.ElementType} params.component component to render
 * @param {string} fallbackUri url to forward when user i failed to authenticate
 */
function PrivateRoute({ component: Component, fallbackUri, ...rest }) {
  const { current, isLoading, error } = useSelector((state) => state.users)
  const dispatch = useDispatch()

  useEffect(() => {
    const accessToken = tokenSerializer.deserializeToken()
    const getUserAsync = async () => {
      dispatch(actions.getUser.START())
      try {
        const { user } = await apiClient.post("auth", { body: { accessToken } })
        dispatch(actions.getUser.SUCCESS(user))
      } catch (e) {
        dispatch(actions.getUser.ERROR(e.message))
      }
    }
    current || getUserAsync()
  }, [current])

  return isLoading ? null : current ? (
    <Component {...rest} />
  ) : error ? (
    <Redirect to={fallbackUri} noThrow />
  ) : null
}

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  fallbackUri: PropTypes.string.isRequired,
}

export default PrivateRoute
