import React from "react"
import PropTypes from "prop-types"
import { Container, makeStyles } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: theme.spacing(180),
  },
}))

/**
 * Default layout to used to align items etc
 * @param {node} props.children component's children
 */
function Layout({ children, ...props }) {
  const classes = useStyles()

  return (
    <Container maxWidth="md" className={classes.root} {...props}>
      {children}
    </Container>
  )
}

Layout.propTypes = {
  children: PropTypes.element.isRequired,
}

export default Layout
