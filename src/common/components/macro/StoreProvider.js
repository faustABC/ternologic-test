import React, { useReducer, createContext, useContext, useMemo } from "react"
import PropTypes from "prop-types"

/**
 * Context to store global state
 */
const StoreContext = createContext({ store: {}, dispatch: null })

/**
 * StoreProvider provides store via React.Context API
 * @param {reducer} props.rootReducer reducer to be passed into root reducer
 * @param {React.ElementType} props.children children to render
 */
function StoreProvider({ rootReducer, children }) {
  const [store, dispatch] = useReducer(
    rootReducer,
    rootReducer(undefined, { type: null })
  )

  return (
    <StoreContext.Provider value={{ store, dispatch }}>
      {children}
    </StoreContext.Provider>
  )
}

StoreProvider.propTypes = {
  rootReducer: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
}

/**
 * Hook returns dispatch.
 * With help of it it's possible to update global state
 */
export const useDispatch = () => {
  const { dispatch } = useContext(StoreContext)
  return dispatch
}

/**
 *
 * @param {Function} selector selector get store part
 * @example
 *
 * const users = useSelector(state => state.users)
 */
export const useSelector = (selector) => {
  const { store } = useContext(StoreContext)
  const selectorValue = useMemo(() => selector(store), [selector, store])

  return selectorValue
}

export default StoreProvider
