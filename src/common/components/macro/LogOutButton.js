import React from "react"
import { IconButton } from "@material-ui/core"
import LogOutIcon from "@material-ui/icons/ExitToApp"
import PropTypes from "prop-types"
import { useDispatch } from "./StoreProvider"
import { actions } from "common"
import { tokenSerializer } from "../../../App"

/**
 * Button used to log out
 * @param {object} props.iconButtonProps props to IconButton component
 */
function LogOutButton({ iconButtonProps }) {
  const dispatch = useDispatch()
  const onClick = () => {
    tokenSerializer.serializeToken("")
    dispatch(actions.logOut())
  }

  return (
    <IconButton {...iconButtonProps} onClick={onClick}>
      <LogOutIcon />
    </IconButton>
  )
}

LogOutButton.propTypes = {
  iconButtonProps: PropTypes.object,
}

export default LogOutButton
