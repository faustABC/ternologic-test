import { IconButton, InputAdornment } from "@material-ui/core"
import React, { useCallback, useState } from "react"
import TextInput from "./Input"
import { Visibility, VisibilityOff } from "@material-ui/icons"

/**
 * PasswordInput is general password input with show/hide value functionality
 * @param {object} textInputProps props will be passed to TextField component
 */
function PasswordInput(textInputProps) {
  const [showPassword, setShowPassword] = useState(false)

  const togglePassword = useCallback(
    () => setShowPassword((isShown) => !isShown),
    [setShowPassword]
  )

  return (
    <TextInput
      {...textInputProps}
      InputProps={{
        type: showPassword ? "text" : "password",
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={togglePassword}
              edge="end"
            >
              {showPassword ? <Visibility /> : <VisibilityOff />}
            </IconButton>
          </InputAdornment>
        ),
      }}
    />
  )
}

PasswordInput.propTypes = TextInput.propTypes

export default PasswordInput
