import React from "react"
import PropTypes from "prop-types"
import {
  CircularProgress,
  makeStyles,
  Button as MaterialButton,
} from "@material-ui/core"

const useStyles = makeStyles({
  buttonProgress: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
  buttonLoading: {
    color: "transparent !important",
  },
})

/**
 * Common button for general purposes
 * @param {React.ElementType} props.children nodes to be rendered as children
 * @param {boolean} props.isLoading indicated if button is in loading state
 * @param {string} props.className MaterialButton className
 * @param {...object} buttonProps rest props will be passed to MaterialButton component
 */
function Button({ children, isLoading = false, className, ...buttonProps }) {
  const classes = useStyles()
  return (
    <MaterialButton
      className={[isLoading && classes.buttonLoading, className].join(" ")}
      {...buttonProps}
    >
      {children}
      {isLoading && (
        <CircularProgress size={24} className={classes.buttonProgress} />
      )}
    </MaterialButton>
  )
}

Button.propTypes = {
  children: PropTypes.node,
  isLoading: PropTypes.bool,
  className: PropTypes.string,
}

export default Button
