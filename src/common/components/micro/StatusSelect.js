import React from "react"
import TextInput from "./Input"
import Status from "./Status"
import PropTypes from "prop-types"
import { makeStyles, MenuItem } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  root: {
    height: theme.spacing(2),
  },
  select: {
    height: theme.spacing(7),
    padding: 0,
    paddingLeft: theme.spacing(2),
    display: "flex",
    alignItems: "center",
  },
}))

/**
 * Select where user can choose status
 * @param {{color: string, text: string}[]} params.items select items to choose from
 * @param {string} params.[value] current select value
 * @param {Function} params.onChange function triggered when user selects another item
 * @param {...object} textInputProps rest props will be passed to the TextInput
 */
function StatusSelect({ items, value = "", onChange, ...textInputProps }) {
  const classes = useStyles()
  const renderValue = (v) => <Status {...items.find((item) => item.id === v)} />

  return (
    <TextInput
      {...textInputProps}
      value={value}
      onChange={onChange}
      select
      SelectProps={{
        renderValue,
        onChange,
        classes: { root: classes.root, select: classes.select },
      }}
    >
      {items.map((item) => (
        <MenuItem key={item.id} value={item.id}>
          <Status {...item} />
        </MenuItem>
      ))}
    </TextInput>
  )
}

StatusSelect.propTypes = {
  items: PropTypes.arrayOf(PropTypes.exact(Status.propTypes)),
  onChange: PropTypes.func.isRequired,
  value: PropTypes.any,
}

export default StatusSelect
