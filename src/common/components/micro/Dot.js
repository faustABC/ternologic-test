import React from "react"
import { makeStyles } from "@material-ui/core"
import PropTypes from "prop-types"

const useStyles = makeStyles((theme) => ({
  root: {
    width: theme.spacing(),
    height: theme.spacing(),
    borderRadius: theme.spacing(),
  },
}))

/**
 * Dot used for status displaying
 * @param {string} props.className root className
 * @param {...object} props.rest rest props will be passed to the root element
 */
function Dot({ className, ...rest }) {
  const classes = useStyles()

  return <div {...rest} className={[className, classes.root].join(" ")} />
}

Dot.propTypes = {
  className: PropTypes.string,
}

export default Dot
