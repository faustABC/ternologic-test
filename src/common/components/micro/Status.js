import React from "react"
import PropTypes from "prop-types"
import { Chip, useTheme } from "@material-ui/core"
import Dot from "./Dot"

/**
 * Status component is like Chip with a dot on the left
 * @param {string} props.text text that will be displayed in Status chip
 * @param {string} props.color dot color
 * @example
 *
 * <Status text="Status with green dot" color="#0F0" />
 */
function Status({ text, color }) {
  const theme = useTheme()

  return (
    <Chip
      label={text}
      variant="outlined"
      avatar={
        <Dot
          style={{
            background: color,
            width: theme.spacing(),
            height: theme.spacing(),
            marginLeft: theme.spacing(1),
          }}
        />
      }
    />
  )
}

Status.propTypes = {
  id: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
}

export default Status
