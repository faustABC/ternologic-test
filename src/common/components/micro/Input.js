import { FormHelperText, makeStyles, TextField } from "@material-ui/core"
import React, { useMemo } from "react"
import PropTypes from "prop-types"

const useStyles = makeStyles((theme) => ({
  helperTextRoot: {
    display: "grid",
    marginTop: theme.spacing(),
    padding: `0 ${theme.spacing(2)}px`,
  },
  errorsInRow: {
    gridTemplateColumns: "1fr 1fr",
  },
  helperText: {
    width: "fit-content",
  },
}))

/**
 * Common Input with error displaying for general purposes
 * @param {React.ElementType} props.children nodes to be rendered as children. Usually text
 * @param {string} props.className MaterialButton className
 * @param {string[]} props.errors errors to display
 * @param {boolean} props.errorsInRow should errors be displayed in one row or in 2
 * @param {string[]} props.persistentRules rules which will be shown always
 * @param {...object} inputProps rest props will be passed to TextField component
 */
function TextInput({
  className,
  errors,
  persistentRules,
  errorsInRow,
  children,
  ...inputProps
}) {
  const classes = useStyles()
  const isError = useMemo(() => !!errors.length, [errors])
  const errorMap = errors.reduce((acc, curr) => ({ ...acc, [curr]: true }), {})
  const errorsToShow = persistentRules || errors

  return (
    <span className={className}>
      <TextField error={isError} {...inputProps}>
        {children}
      </TextField>
      <span
        className={[
          classes.helperTextRoot,
          errorsInRow && classes.errorsInRow,
        ].join(" ")}
      >
        {errorsToShow.map((error) => (
          <FormHelperText
            error={!!errorMap[error]}
            key={error}
            id={error}
            className={classes.helperText}
          >
            {error}
          </FormHelperText>
        ))}
      </span>
    </span>
  )
}

TextInput.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.any,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  persistentRules: PropTypes.arrayOf(PropTypes.string),
  label: PropTypes.string,
  className: PropTypes.string,
  errors: PropTypes.arrayOf(PropTypes.string),
  errorsInRow: PropTypes.bool,
  children: PropTypes.node,
}

export default TextInput
