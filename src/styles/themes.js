import blue from "@material-ui/core/colors/blue"
import grey from "@material-ui/core/colors/grey"

/** Light theme for material components */
export const lightTheme = {
  props: {
    MuiTextField: {
      variant: "outlined",
      fullWidth: true,
    },
  },
  palette: {
    primary: {
      main: blue[500],
    },
    secondary: {
      main: grey[100],
    },
  },
}
