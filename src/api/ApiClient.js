import { HTTPError } from "errors"
import { REQUEST_DELAY } from "configuration"

/**
 * wraps promise @param promiseLike to wait @param delay milliseconds
 * @param {Promise} promiseLike promise with data to extract
 * @param {number} delay delay in ms to wait
 */
const withDelay = (promiseLike, delay) =>
  new Promise((resolve, reject) => {
    setTimeout(async () => {
      try {
        const result = await promiseLike
        resolve(result)
      } catch (e) {
        reject(e)
      }
    }, delay)
  })

/**
 *  Client for communication with backend
 * @class
 */
export class ApiClient {
  /**
   *
   * @param {string} baseApiUrl url to backend
   * @param {Function} fetch fetch function (can be browser or from 3-rd party library)
   * @param {number} [delay] delay to wait until resolve query result
   * @param {object} fetchOptions options passed to @param fetch
   */
  constructor(baseApiUrl, fetch, delay = REQUEST_DELAY, fetchOptions) {
    this.baseApiUrl = baseApiUrl
    this.fetch = fetch
    this.delay = delay
    this.fetchOptions = {
      credentials: "same-origin",
      mode: "same-origin",
      ...fetchOptions,
    }
  }

  async makeRequest(method, urlParamsString, { body, searchParams } = {}) {
    const url = new URL(this.baseApiUrl)
    Object.entries(searchParams || {}).forEach(([key, value]) =>
      url.searchParams.set(key, value)
    )
    url.pathname += "/" + urlParamsString
    const request = await withDelay(
      this.fetch(url.href, {
        method,
        body: body && JSON.stringify(body),
        ...this.fetchOptions,
      }),
      this.delay
    )

    if (!request.ok) {
      throw new HTTPError(this.baseApiUrl, request.status, request.statusText)
    }

    return request.json()
  }

  async get(urlParamsString, { searchParams } = {}) {
    return this.makeRequest("GET", urlParamsString, { searchParams })
  }

  async post(urlParamsString, { body, searchParams } = {}) {
    return this.makeRequest("POST", urlParamsString, { body, searchParams })
  }

  async put(urlParamsString, { body, searchParams } = {}) {
    return this.makeRequest("PUT", urlParamsString, { body, searchParams })
  }

  async delete(urlParamsString) {
    return this.makeRequest("DELETE", urlParamsString)
  }
}
