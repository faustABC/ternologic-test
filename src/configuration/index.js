/** Static path configuration for routes */
export const ROUTING = {
  ROOT: "/",
  SIGN_IN: "/sign-in",
  SIGN_UP: "/sign-up",
  AUTH_REQUIRED: "/auth-required",
}

/** Project statuses by owner */
export const AVAILABLE_STATUSES = {
  ALL: "ALL",
  MY: "MY",
  ACQUIRED: "ACQUIRED",
  DRAFTED: "DRAFTED",
}

/** General delay of all requests to the backend */
export const REQUEST_DELAY = 1000

/** Debounced delay of search projects input */
export const FILTER_DEBOUNCE = 400

/** Default name of access token */
export const ACCESS_TOKEN_NAME = "accessToken"
